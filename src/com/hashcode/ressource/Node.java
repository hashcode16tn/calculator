package com.hashcode.ressource;

import java.util.ArrayList;

public class Node {
	private Integer x ;
	private Integer y ;
	private Integer weight ;
	private Integer visited ;
	private ArrayList<Node> children ;
	private Integer depth ;

	public Node(Integer x, Integer y, Integer depth){
		this.x = x;
		this.y = y;
		weight = 0;
		this.depth = depth ;
		children = new ArrayList<Node>() ;
	}

	public void addChild(Integer x, Integer y){
		children.add(new Node(x, y, depth++)) ;
	}

	public ArrayList<Node> getChildren() {
		return children ;
	}

	public Integer getVisited(){
		// 1 // 2 // 3
		return visited ;
	}

	public void setVisited(Integer v){
		visited = v ;
	}

	public Integer getX() {
		return x ;
	}
	public Integer getY() {
		return y ;
	}
	public Integer getDepth(){
		return depth ;
	}
}
