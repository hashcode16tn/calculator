package com.hashcode.src;

import java.util.ArrayList;

import com.hashcode.ressource.File;

public class VarVal {

	ArrayList<String> listofVariables = new ArrayList<String>();

	public VarVal(File file) {
		int size = file.nbrOfLines();
		for (int i = 0; i < size; i++)
			listofVariables.add(file.getLine(i));

	}


	public int getValofVar(String x) {
		int resultat = 0;
		for (int i = 0; i < listofVariables.size(); i++) {
			if (listofVariables.get(i).indexOf(x) != -1) {
				String wert = listofVariables.get(i).substring(2);
				resultat = Integer.parseInt(wert);
			}
		}
		return resultat;

	}

}

