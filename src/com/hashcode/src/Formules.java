package com.hashcode.src;

import java.util.ArrayList;

import com.hashcode.ressource.File;



public class Formules {
	
	//private String formOfIndex ;

	private ArrayList<String> listOfFormules ;
	
	public Formules(File myFile){
		ArrayList<String> formules = new ArrayList<String>() ;
		String formule ;
		for(int i=0;i<myFile.nbrOfLines();i++){
			formule = myFile.getLine(i) ;	
			formules.add(formule) ;
			
			formule = "";
		}
		this.listOfFormules = formules ;
		
		
	}
	
	
	public ArrayList<String> getListOfFormule() {
		
	    return listOfFormules;	
	}
	public String formOfIndex(int i) {
		 
		return getListOfFormule().get(i);
		
}
}
