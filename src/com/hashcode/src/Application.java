package com.hashcode.src;

import java.util.ArrayList;
import java.util.Iterator;

import com.hashcode.ressource.File;
import com.hashcode.ressource.FileManager;

public class Application {

	public static void main(String[] args) {

		FileManager fm = new FileManager();

		Formules implFormules = new Formules(fm.getFile("formules.txt"));
		VarVal variables = new VarVal(fm.getFile("variables.txt"));
		Calculate implCalcul = new Calculate();

		ArrayList<String> formules = implFormules.getListOfFormule();
		File output = new File();

		Iterator<String> itFormule = formules.iterator();

		while (itFormule.hasNext()) {
			String temp = itFormule.next();
			output.addLine(implCalcul.getResult(temp, variables));
		}

		fm.printFile(output, "output.txt");

	}
}
