package com.hashcode.src;

import java.util.ArrayList;

public class Calculate {

	public String getResult(String expr, VarVal values){
		ArrayList<String> varsInExpr = getVarsInExpr(expr) ;
		for(String v : varsInExpr){
			expr = expr.replaceAll(v, new Integer(values.getValofVar(v)).toString()) ;
		}

		Integer r = evaluate(expr) ;

		String result = expr + " = " + r.toString() ;
		return result ;

	}
	private Integer evaluate(String expr) {
		// TODO Auto-generated method stub
		for(int i = 0 ; i < expr.length() ; i++){
			if(expr.charAt(i) == '*'){
				String init = expr ;
				Integer l = new Integer(new Character(expr.charAt(i-1)).toString()) ;
				Integer r = new Integer(new Character(expr.charAt(i+1)).toString()) ;
				Integer s = l * r ;
				// new expr

				if(i - 2 >= 0)
					expr = init.substring(0, i-2);
				else
					expr = "" ;

				expr += s.toString() ;
				if(init.length() >= i+2){
					expr +=  init.substring(i+2, init.length()) ;
				}
				i = 0 ;
			}
		}
		for(int i = 0 ; i < expr.length() ; i++){
			if(expr.charAt(i) == '/'){
				String init = expr ;
				Integer l = new Integer(new Character(expr.charAt(i-1)).toString()) ;
				Integer r = new Integer(new Character(expr.charAt(i+1)).toString()) ;
				Integer s = l / r ;
				// new expr
				if(i - 2 >= 0)
					expr = init.substring(0, i-2);
				else
					expr = "" ;

				expr += s.toString() ;

				if(init.length() >= i+2){
					expr +=  init.substring(i+2, init.length()) ;
				}
				i = 0 ;
			}
		}
		for(int i = 0 ; i < expr.length() ; i++){
			if(expr.charAt(i) == '+'){
				String init = expr ;
				Integer l = new Integer(new Character(expr.charAt(i-1)).toString()) ;
				Integer r = new Integer(new Character(expr.charAt(i+1)).toString()) ;
				Integer s = l + r ;
				// new expr
				if(i - 2 >= 0)
					expr = init.substring(0, i-2);
				else
					expr = "" ;

				expr += s.toString() ;

				if(init.length() >= i+2){
					expr +=  init.substring(i+2, init.length()) ;
				}
				i = 0 ;
			}
		}
		for(int i = 0 ; i < expr.length() ; i++){
			if(expr.charAt(i) == '-'){
				String init = expr ;
				Integer l = new Integer(new Character(expr.charAt(i-1)).toString()) ;
				Integer r = new Integer(new Character(expr.charAt(i+1)).toString()) ;
				Integer s = l - r ;
				// new expr
				if(i - 2 >= 0)
					expr = init.substring(0, i-2);
				else
					expr = "" ;

				expr += s.toString() ;

				if(init.length() >= i+2){
					expr +=  init.substring(i+2, init.length()) ;
				}
				i = 0 ;
			}
		}
		System.out.println(expr) ;
		return new Integer(expr);
	}
	private ArrayList<String> getVarsInExpr(String ex) {
		// TODO Auto-generated method stub
		ArrayList<String> ls = new ArrayList<String>() ;
		for(int i = 0 ; i < ex.length() ; i++){
			if((new Character(ex.charAt(i)).toString().matches("[a-zA-Z]+"))){ // var
				if(!ls.contains(new Character(ex.charAt(i)).toString())){
					ls.add(new Character(ex.charAt(i)).toString()) ;
				}
			}
		}
		return ls;
	};
}
